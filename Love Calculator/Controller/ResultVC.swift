//
//  ResultVC.swift
//  Love Calculator
//
//  Created by Мухаммад Самарахонов on 02/02/23.
//

import UIKit

class ResultVC: UIViewController {
    
    var sname: String = ""
    var fname: String = ""
    var History: [CoreData] = []
    
    
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var firstName: UILabel!
    @IBOutlet weak var secondName: UILabel!
    @IBOutlet weak var resultText: UILabel!
    @IBOutlet weak var actualResult: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getResult()
        History = DataManager.shared.budgets()
    }
    
    @IBAction func cancelBtn(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    func getResult() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            Loader.start()
        }
        GetPercentageAPI.getPercentage(sname: sname, fname: fname) { data in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                Loader.stop()
            }
            guard let data = data else {return}
            if data.percentage >= 80 {
                self.mainImage.image = UIImage(named: "happy")
            }
            else if  data.percentage >= 50 && data.percentage < 80 {
                self.mainImage.image = UIImage(named: "normal")
            }
            else if data.percentage < 50 {
                self.mainImage.image = UIImage(named: "sad")
            }
            
            self.firstName.text = data.fname
            self.secondName.text = data.sname
            self.resultText.text = data.result
            self.actualResult.text = String(data.percentage)
            let coreDara = DataManager.shared.history(fname: self.firstName.text!, sname: self.secondName.text!, percent: Int16(self.actualResult.text!)!, text: self.resultText.text!)
            self.History.append(coreDara)
            DataManager.shared.save()
        }
    }
}
