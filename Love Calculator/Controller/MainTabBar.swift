//
//  MainTabBar.swift
//  Love Calculator
//
//  Created by Мухаммад Самарахонов on 02/02/23.
//

import UIKit

class MainTabBar: UITabBarController {
    
    private let vc1 = UINavigationController(rootViewController: CalculateVC())
    private let vc2 = UINavigationController(rootViewController: SettingVC())

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            vc1.tabBarItem = UITabBarItem(title: "Home", image: UIImage(systemName: "heart"), selectedImage: UIImage(systemName: "heart.fill"))
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 13.0, *) {
            vc2.tabBarItem = UITabBarItem(title: "History", image: UIImage(systemName: "arrow.clockwise.heart"), selectedImage: UIImage(systemName: "arrow.clockwise.heart.fill"))
        } else {
            // Fallback on earlier versions
        }
        viewControllers = [vc1, vc2]
        ChangeRadius0fTabbar()
    }
    
    func ChangeRadius0fTabbar () {
        self.tabBar.layer.masksToBounds = true
        self.tabBar.isTranslucent = true
        self.tabBar.layer.cornerRadius = 30
        self.tabBar.layer.maskedCorners = [. layerMinXMinYCorner, .layerMaxXMinYCorner]
        self.tabBar.unselectedItemTintColor = .systemPink
        if #available(iOS 13.0, *) {
            self.tabBar.backgroundColor = .systemBackground
        } else {
            // Fallback on earlier versions
        }
        self.tabBar.tintColor = .systemPink
    }
    
}
