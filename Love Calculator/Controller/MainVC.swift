//
//  MainVC.swift
//  Love Calculator
//
//  Created by Мухаммад Самарахонов on 02/02/23.
//

import UIKit

class MainVC: UIViewController {

    lazy var paging: CustomPageControl = {
        let pc = CustomPageControl()
        pc.currentPage = 0
        pc.numberOfPages = 3
        pc.pageIndicatorTintColor = .systemPink
        pc.currentPageIndicatorTintColor = #colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1)
        pc.currentPageImage = UIImage(named: "OvalSelect")
        pc.otherPagesImage = UIImage(named: "OvalNoSelect")
        return pc
    }()
    
    lazy var backButton: BlueButton = {
        let button = BlueButton()
        button.title = "Next"
        button.isCanTapped = true
        button.button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return button
    }()
    
    
    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.delegate = self
            scrollView.isScrollEnabled = true
        }
    }

    
    
//    MARK: view did load
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonAdder()
    }

}

extension MainVC {
    
//    MARK: bottom button
    func buttonAdder(){
        
        view.addSubview(backButton)
        view.addSubview(paging)
        
        backButton.snp.makeConstraints { make in
            make.left.right.equalTo(view).inset(24)
            make.bottom.equalTo(view).inset(42)
            make.height.equalTo(48)
        }
        
        paging.snp.makeConstraints { make in
            make.bottom.equalTo(backButton).inset(100)
            make.centerX.equalTo(view)
        }
        
    }
    
//    MARK: bottom buttons action
    @objc func buttonTapped(){
        
        if scrollView.contentOffset.x < view.frame.width * 2  {
            scrollView.setContentOffset(CGPoint(x: scrollView.contentOffset.x + view.frame.width, y: 0), animated: true)
        } else {
//            let vc = UINavigationController(rootViewController: MainTabBar())
            let vc = MainTabBar()
            vc.modalPresentationStyle = .overFullScreen
            present(vc, animated: true, completion: nil)
        }
        
    }
    
}


extension MainVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if round(scrollView.contentOffset.x/UIScreen.main.bounds.width) == 0 {
            paging.currentPage = 0
            backButton.button.setTitle("next", for: .normal)
        } else if round(scrollView.contentOffset.x/UIScreen.main.bounds.width) == 1 {
            paging.currentPage = 1
            backButton.button.setTitle("next", for: .normal)
        } else if round(scrollView.contentOffset.x/UIScreen.main.bounds.width) == 2 {
            paging.currentPage = 2
            backButton.button.setTitle("Begin Calculating", for: .normal)
        }
        
    }
    
}


class CustomPageControl: UIPageControl {
    
    @IBInspectable var currentPageImage: UIImage?
    @IBInspectable var otherPagesImage: UIImage?
    
    override var numberOfPages: Int {
        didSet {
            updateDots()
        }
    }
    
    override var currentPage: Int {
        didSet {
            updateDots()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        pageIndicatorTintColor = .clear
        currentPageIndicatorTintColor = .clear
        clipsToBounds = false
    }
    
    private func updateDots() {
        
        for (index, subview) in subviews.enumerated() {
            let imageView: UIImageView
            if let existingImageview = getImageView(forSubview: subview) {
                imageView = existingImageview
            } else {
                imageView = UIImageView(image: otherPagesImage)
                
                imageView.center = subview.center
                subview.addSubview(imageView)
                subview.clipsToBounds = false
            }
            imageView.image = currentPage == index ? currentPageImage : otherPagesImage
        }
    }
    
    private func getImageView(forSubview view: UIView) -> UIImageView? {
        if let imageView = view as? UIImageView {
            return imageView
        } else {
            let view = view.subviews.first { (view) -> Bool in
                return view is UIImageView
            } as? UIImageView
            
            return view
        }
    }
}

