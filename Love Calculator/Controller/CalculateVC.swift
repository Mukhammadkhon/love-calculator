//
//  CalculateVC.swift
//  Love Calculator
//
//  Created by Мухаммад Самарахонов on 02/02/23.
//

import UIKit

class CalculateVC: UIViewController {

    @IBOutlet weak var firstField: UITextField!{
        didSet{
            let bottomLine = CALayer()
            bottomLine.frame = CGRect(x: 0.0, y: firstField.frame.height - 1, width: firstField.frame.width, height: 2.0)
            bottomLine.backgroundColor = UIColor.systemPink.cgColor
            firstField.borderStyle = UITextField.BorderStyle.none
            firstField.layer.addSublayer(bottomLine)
        }
    }
    @IBOutlet weak var secondField: UITextField!{
        didSet {
            let bottomLine = CALayer()
            bottomLine.frame = CGRect(x: 0.0, y: secondField.frame.height - 1, width: secondField.frame.width, height: 2.0)
            bottomLine.backgroundColor = UIColor.systemPink.cgColor
            secondField.borderStyle = UITextField.BorderStyle.none
            secondField.layer.addSublayer(bottomLine)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Love Calculator"
        self.hideKeyboardWhenTappedAround()
    }
    
    @IBAction func checkTapped(_ sender: Any) {
        let mainVC = ResultVC()
        mainVC.fname = self.firstField.text ?? "John"
        mainVC.sname = self.secondField.text ?? "Alice"
        let vc = UINavigationController(rootViewController: mainVC)
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: true, completion: nil)
    }
    
}
