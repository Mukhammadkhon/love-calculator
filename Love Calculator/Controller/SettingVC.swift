//
//  SettingVC.swift
//  Love Calculator
//
//  Created by Мухаммад Самарахонов on 03/02/23.
//

import UIKit

class SettingVC: UIViewController {
    
    var historyData: [CoreData] = []

    @IBOutlet weak var historyTable: UITableView!{
        didSet{
            historyTable.delegate = self
            historyTable.dataSource = self
            historyTable.allowsSelection = false
            historyTable.register(UINib(nibName: "HistoryTVC", bundle: nil), forCellReuseIdentifier: "HistoryTVC")
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
        title = "History"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        historyData = DataManager.shared.budgets().reversed()
        historyTable.reloadData()
    }

}
extension SettingVC: UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        historyData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTVC") as! HistoryTVC
        cell.updateTVC(data: historyData[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let object = historyData[indexPath.row]
            DataManager.shared.delete(object)
            historyData.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    
}
