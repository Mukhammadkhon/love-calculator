//
//  DataManager.swift
//  Love Calculator
//
//  Created by Мухаммад Самарахонов on 07/02/23.
//

import Foundation
import CoreData

class DataManager {
    
    static let shared = DataManager()
    
    lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "DataModel")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    func history(fname:String , sname: String , percent: Int16 , text: String) -> CoreData {
        let hist = CoreData(context: persistentContainer.viewContext)
        hist.fname = fname
        hist.sname = sname
        hist.percent = percent
        hist.text = text
        return hist
    }
    
    func budgets () -> [CoreData] {
        let request: NSFetchRequest<CoreData> = CoreData.fetchRequest ()
        var fetchedBudgets: [CoreData] = []
        do {
            fetchedBudgets = try persistentContainer.viewContext.fetch(request)
        } catch {
            print("Error fetching budgets")
            
        }
        
        return fetchedBudgets
        
    }
    
    // MARK: - Core Data Saving support
    
    func save() {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func delete(_ object: CoreData) {
        let context = persistentContainer.viewContext
        context.delete(object)
        save()
    }
}
