//
//  PercentageDM.swift
//  Love Calculator
//
//  Created by Мухаммад Самарахонов on 04/02/23.
//

import Foundation
import Alamofire
import SwiftyJSON

struct PercentageDM {
    var fname:String
    var sname:String
    var percentage:Int
    var result:String
    
    init (json:JSON) {
        fname = json["fname"].stringValue
        sname = json["sname"].stringValue
        percentage = json["percentage"].intValue
        result = json["result"].stringValue
    }
}
