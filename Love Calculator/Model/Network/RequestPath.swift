//
//  RequestPath.swift
//  Love Calculator
//
//  Created by Мухаммад Самарахонов on 03/02/23.
//

import Foundation

enum RequestPath {
    
    case getPercentage

    var path: String {
        switch self {
        case .getPercentage:
            return "/getPercentage"
        }
    }
    
}
