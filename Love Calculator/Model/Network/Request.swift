//
//  Request.swift
//  Love Calculator
//
//  Created by Мухаммад Самарахонов on 03/02/23.
//

import Foundation
import SwiftyJSON
import Alamofire

class Request {
    static var commonHeader: HTTPHeaders {
        [
            "X-RapidAPI-Key": "c0c537e5f0msh204d3fc546576c6p101d64jsn15aebce75b4d",
            "X-RapidAPI-Host": "love-calculator.p.rapidapi.com"
        ]
    }
    
    class func request(path: RequestPath, method: HTTPMethod, params: Parameters, encoding: ParameterEncoding = URLEncoding.default, completion: @escaping (JSON?) -> Void) {
        AF.request("https://love-calculator.p.rapidapi.com" + path.path, method: method, parameters: params, encoding: encoding, headers: commonHeader).response { response in
            
            guard let data = response.data else { completion(nil) ; return }
            switch response.result {
            case .success(_):
                let jsonData = JSON(data)
                completion(jsonData)
            case .failure(_):
                completion(nil)
            }
        }
    }
}
