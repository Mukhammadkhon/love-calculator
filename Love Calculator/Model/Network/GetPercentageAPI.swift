//
//  GetPercentageAPI.swift
//  Love Calculator
//
//  Created by Мухаммад Самарахонов on 03/02/23.
//

import Foundation
import Alamofire
import SwiftyJSON

class GetPercentageAPI {
    
    class func getPercentage(sname: String , fname:String , completion: @escaping (PercentageDM?) -> Void) {
        
        let params: Parameters = [
            "sname": sname ,
            "fname": fname
        ]
        Request.request(path: .getPercentage, method: .get, params: params) { data in
            guard let data = data else {return}
            let singleProduct = PercentageDM(json: data)
            completion(singleProduct)
        }
    }
}
