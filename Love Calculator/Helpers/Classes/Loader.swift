//
//  Loader.swift
//  Love Calculator
//
//  Created by Мухаммад Самарахонов on 04/02/23.
//

import Foundation
import Lottie

public class Loader {
    
    ///Shows custom Alert for a while
    class func start(backgroundColor: UIColor? = nil) {
        
        let height = UIScreen.main.bounds.width*150/375
        let loadV = LoaderView()
        loadV.tag = 995
        
        
        loadV.frame = UIScreen.main.bounds
        loadV.isUserInteractionEnabled = true
        let customView = LottieAnimationView()
        customView.animation = LottieAnimation.named("99936-heart")
        customView.animationSpeed = 1.5
        customView.loopMode = .loop
        customView.play()
        customView.frame = CGRect(x: 0, y: 0, width:UIScreen.main.bounds.width/2, height: UIScreen.main.bounds.width/2)
                
        loadV.animationView.addSubview(customView)
        customView.translatesAutoresizingMaskIntoConstraints = false
        customView.centerXAnchor.constraint(equalTo: loadV.animationView.centerXAnchor).isActive = true
        customView.centerYAnchor.constraint(equalTo: loadV.animationView.centerYAnchor).isActive = true
        customView.heightAnchor.constraint(equalToConstant: height).isActive = true
        customView.widthAnchor.constraint(equalToConstant: height).isActive = true
        customView.layer.opacity = 0.8
        
        if let window = UIApplication.shared.windows.first {
            if let v = window.viewWithTag(995) {
                v.removeFromSuperview()
            } else {
                window.addSubview(loadV)
            }
        }
    }
    
    class func stop() {
        DispatchQueue.main.async {
            guard let views = UIApplication.shared.keyWindow else { return }
            for i in views.subviews {
                if i.tag == 995 {
                    if let view = i as? LoaderView {
                        UIView.animate(withDuration: 0.3) {
                            view.animationView.alpha = 0
                        } completion: { _ in
                            view.removeFromSuperview()
                        }
                    }
                }
            }
        }
    }
}


class LoaderView: UIView {
    
    var animationView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let height = UIScreen.main.bounds.width*600/375
        animationView.clipsToBounds = true
        animationView.layer.cornerRadius = 21
        self.addSubview(animationView)
        animationView.translatesAutoresizingMaskIntoConstraints = false
        animationView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        animationView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        animationView.heightAnchor.constraint(equalToConstant: height).isActive = true
        animationView.widthAnchor.constraint(equalToConstant: height).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
