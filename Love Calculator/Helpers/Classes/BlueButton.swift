//
//  BlueButton.swift
//  Love Calculator
//
//  Created by Мухаммад Самарахонов on 02/02/23.
//

import Foundation
import SnapKit
import UIKit


class BlueButton: UIView {
    
    var title: String? {
        get {
            return button.titleLabel?.text ?? "empty"
        }
        set {
            if let value = newValue {
                button.setTitle(value, for: .normal)
                self.addSubview(button)
                button.snp.makeConstraints { make in
                    make.edges.equalTo(self)
                }
                clipsToBounds = true
                layer.cornerRadius = 12
            }
        }
    }
    
    var isCanTapped: Bool? {
        get {
            backgroundColor = .systemPink
            button.isUserInteractionEnabled = true
            return true
        }
        set {
            if let value = newValue {
                if value {
                    backgroundColor = .systemPink
                    button.isUserInteractionEnabled = true; return
                }
                backgroundColor = #colorLiteral(red: 0.9254904389, green: 0.9254902005, blue: 0.9211822152, alpha: 1)
                button.isUserInteractionEnabled = false
            }
        }
    }
    
    let button: UIButton = {
        let button = UIButton()
        button.tintColor = .white
        return button
    }()
    
}

