//
//  LaunchScreen.swift
//  Love Calculator
//
//  Created by Мухаммад Самарахонов on 02/02/23.
//

import UIKit
import Lottie

class LaunchScreen: UIViewController {
    
    @IBOutlet weak var animationView: LottieAnimationView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lottieAnimation()
        //        UIWindow.goToMainPage()
    }
    
    func lottieAnimation(){
        
        let animation = LottieAnimationView(name: "8307-love-icon-animation")
        animation.frame = CGRect(x: 0, y: 0, width: 350, height: 350)
        animation.contentMode = .scaleAspectFit
        animationView.addSubview(animation)
        animation.play { _ in
            UIView.animate(withDuration: 1, delay: 2, options: .curveEaseIn) {
                self.animationView.transform = .init(scaleX: 100, y: 100)
                self.animationView.alpha = 0
            } completion: { _ in
                self.rootToVC()
            }
        }
    }
    
    @objc func rootToVC(){
        UIWindow.goToMainPage()
        
    }
    
}
