//
//  HistoryTVC.swift
//  Love Calculator
//
//  Created by Мухаммад Самарахонов on 04/02/23.
//

import UIKit

class HistoryTVC: UITableViewCell {

    @IBOutlet weak var fName: UILabel!
    @IBOutlet weak var sName: UILabel!
    @IBOutlet weak var result: UILabel!
    @IBOutlet weak var actualResult: UILabel!
    @IBOutlet weak var resultImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateTVC(data: CoreData) {
        fName.text = data.fname
        sName.text = data.sname
        result.text = data.text
        actualResult.text = String(data.percent)
        if data.percent >= 80 {
            if #available(iOS 13.0, *) {
                self.resultImage.image = UIImage(systemName: "bolt.heart.fill" )
            } else {
                // Fallback on earlier versions
            }
        }
        else if  data.percent >= 50 && data.percent < 80 {
            if #available(iOS 13.0, *) {
                self.resultImage.image = UIImage(systemName: "suit.heart.fill")
            } else {
                // Fallback on earlier versions
            }
        }
        else if data.percent < 50 {
            if #available(iOS 13.0, *) {
                self.resultImage.image = UIImage(systemName: "heart.slash.fill" )
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
}
